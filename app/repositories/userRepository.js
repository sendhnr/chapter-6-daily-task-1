const { User } = require("../models");

module.exports = {
  create(createArgs) {
    return User.create(createArgs);
  },

  update(id, updateArgs) {
    return User.update(updateArgs, {
      where: {
        id,
      },
    });
  },

  delete(id) {
    return User.destroy({
      where: {
        id
      }
    });
  },

  find(id) {
    return User.findByPk(id);
  },

  findAll() {
    return User.findAll({
      // TAMBAHAN SRII
      attributes: { exclude: ['password'] }
    });
  },

  getTotalUser() {
    return User.count();
  },

  // register
  async registerNewUser(createArgs) {
    console.log('ini adalah repository');

    const user = await User.findOne({
      where: {
        email: createArgs.email,
      }
    });

    // check if user already exist
    if (user && user.email === createArgs.email) {
      throw new Error(`user with email : ${user.email} already taken`);
    }

    return User.create(createArgs);
  },

  login(userArgs) {
    console.log(userArgs);
    return User.findOne({
      where: {
        email: userArgs.email
      }
    });
  }
};
