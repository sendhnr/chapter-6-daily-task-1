const bcrypt = require('bcrypt');

const userService = require("../../../services/userService");

module.exports = {
  list(req, res) {
    userService
      .list()
      .then(({ data, count }) => {
        res.status(200).json({
          status: "OK",
          data: data,
          meta: { total: count },
        });
      })
      .catch((err) => {
        res.status(400).json({
          status: "FAIL",
          message: err.message,
        });
      });
  },

  create(req, res) {
    userService
      .create(req.body)
      .then((post) => {
        res.status(201).json({
          status: "OK",
          data: user,
        });
      })
      .catch((err) => {
        res.status(422).json({
          status: "FAIL",
          message: err.message,
        });
      });
  },

  update(req, res) {
    userService
      .update(req.params.id, req.body)
      .then(() => {
        res.status(200).json({
          status: "OK",
        });
      })
      .catch((err) => {
        res.status(422).json({
          status: "FAIL",
          message: err.message,
        });
      });
  },

  show(req, res) {
    userService
      .get(req.params.id)
      .then((post) => {
        res.status(200).json({
          status: "OK",
          data: post,
        });
      })
      .catch((err) => {
        res.status(422).json({
          status: "FAIL",
          message: err.message,
        });
      });
  },

  destroy(req, res) {
    userService
      .delete(req.params.id)
      .then(() => {
        res.status(204).end();
      })
      .catch((err) => {
        res.status(422).json({
          status: "FAIL",
          message: err.message,
        });
      });
  },

  register(req, res) {
    console.log('ini controller');

    userService
      .register(req.body)
      .then((user) => {
        res.status(201).json({
          status: "OK",
          message: "register success",
          data: user,
        });
      })
      .catch((err) => {
        res.status(422).json({
          status: "FAIL",
          message: err.message,
        });
      });
  },

  login(req, res) {
    console.log('ini controller');
    userService
      .login(req.body)
      .then(async (user) => {
        // compare input pass with password in database
        const passwordCheck = await bcrypt.compare(req.body.password, user.password);
        // check if user not exist
        console.log(user);
        if (!user) {
          res.status(404).json({
            status: "FAIL",
            message: `user with email : ${req.body.email} is not found`,
          });
        } else if (!passwordCheck) {
          res.status(404).json({
            status: "FAIL",
            message: `incorrect password`,
          });
        } else {
          res.status(201).json({
            status: "OK",
            message: "success login",
            data: {
              firstName: user.firstName,
              lastName: user.lastName,
              email: user.email,
              createdAt: user.createdAt,
              updatedAt: user.updatedAt,
            }
          });
        }
      })
      .catch((err) => {
        res.status(422).json({
          status: "FAIL",
          message: err.message,
        });
      });
  }
};
